﻿using System;
using System.Collections.Generic;
using AutoFixture;
using AutoFixture.AutoMoq;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Controllers;
using Xunit;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
    public class SetPartnerPromoCodeLimitAsyncTests
    {
        private readonly Mock<IRepository<Partner>> _repo;
        private readonly PartnersController _controller;
        public SetPartnerPromoCodeLimitAsyncTests()
        {
            var fixture = new Fixture().Customize(new AutoMoqCustomization());
            _repo = fixture.Freeze<Mock<IRepository<Partner>>>();
            _controller = fixture.Build<PartnersController>().OmitAutoProperties().Create();
        }

        public Partner CreateTestPartner()
        {
            return new Partner()
            {
                Id = Guid.NewGuid(),
                Name = "TestPartner",
                IsActive = true,
                NumberIssuedPromoCodes = 10,
                PartnerLimits = new List<PartnerPromoCodeLimit>()
                {
                    new PartnerPromoCodeLimit()
                    {
                        Id = Guid.NewGuid(),
                        CreateDate = DateTime.Now,
                        EndDate = DateTime.Now.AddDays(30),
                        Limit = 100
                    }
                }
            };
        }

        //-------------------------------------------------------------------------------------------
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_PartnerIsNotFound_ReturnNotFound()
        {
            //arrange
            var id = Guid.NewGuid();
            var request = new Fixture().Build<SetPartnerPromoCodeLimitRequest>().Create();
            Partner partner = null;
            _repo.Setup(r => r.GetByIdAsync(id)).ReturnsAsync(partner);

            //act
            var res = await _controller.SetPartnerPromoCodeLimitAsync(id, request);

            //assert
            res.Should().BeAssignableTo<NotFoundResult>();
        }

        //---------------------------------------------------------------------------------------------
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_PartnerIsNotActive_ReturnBadRequest()
        {
            //arrange
            var id = Guid.NewGuid();
            var request = new Fixture().Build<SetPartnerPromoCodeLimitRequest>().Create();
            var partner = new Fixture().Build<Partner>().OmitAutoProperties()
                .With(p => p.IsActive, false)
                .Create();
            _repo.Setup(r => r.GetByIdAsync(id)).ReturnsAsync(partner);

            //act
            var res = await _controller.SetPartnerPromoCodeLimitAsync(id, request);

            //assert
            res.Should().BeAssignableTo<BadRequestObjectResult>("Данный партнер не активен");
        }

        //---------------------------------------------------------------------------------------------
        [Theory]
        [InlineData (true)]
        [InlineData (false)]
        public async void SetPartnerPromoCodeLimitAsync_SetNewLimitWithOpenLimit_IssuedPromoCodesZeroed(bool cancelDTIsNull)
        {
            //arrange
            var id = Guid.NewGuid();
            var fixture = new Fixture();
            var request = fixture.Build<SetPartnerPromoCodeLimitRequest>().Create();
            var partner = CreateTestPartner();
            if(!cancelDTIsNull)
                (partner.PartnerLimits as List<PartnerPromoCodeLimit>)[0].CancelDate = DateTime.Now;
            _repo.Setup(r => r.GetByIdAsync(id)).ReturnsAsync(partner);

            //act
            var res = await _controller.SetPartnerPromoCodeLimitAsync(id, request);

            //assert
            partner.NumberIssuedPromoCodes.Should().Be(cancelDTIsNull ? 0:10);
        }

        //---------------------------------------------------------------------------------------------
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_SetNewLimit_LastActiveLimitMustBeClosed()
        {
            //arrange
            var id = Guid.NewGuid();
            var fixture = new Fixture();
            var request = fixture.Build<SetPartnerPromoCodeLimitRequest>().Create();
            var partner = CreateTestPartner();
            (partner.PartnerLimits as List<PartnerPromoCodeLimit>)[0].CancelDate = null;
            _repo.Setup(r => r.GetByIdAsync(id)).ReturnsAsync(partner);

            //act
            var res = await _controller.SetPartnerPromoCodeLimitAsync(id, request);

            //assert
            (partner.PartnerLimits as List<PartnerPromoCodeLimit>)[0].CancelDate.Should().NotBeNull();
        }

        //---------------------------------------------------------------------------------------------
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_LimitIsZero_ReturnBadRequest()
        {
            //arrange
            var id = Guid.NewGuid();
            var fixture = new Fixture();
            var request = fixture.Build<SetPartnerPromoCodeLimitRequest>()
                .With(r=>r.Limit, 0)
                .Create();
            var partner = CreateTestPartner();
            _repo.Setup(r => r.GetByIdAsync(id)).ReturnsAsync(partner);

            //act
            var res = await _controller.SetPartnerPromoCodeLimitAsync(id, request);

            //assert
            res.Should().BeAssignableTo<BadRequestObjectResult>("Лимит должен быть больше 0");
        }

        //---------------------------------------------------------------------------------------------
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_LimitIsOk_SureNewLimitStored()
        {
            //arrange
            var id = Guid.NewGuid();
            var fixture = new Fixture();
            var request = fixture.Build<SetPartnerPromoCodeLimitRequest>()
                .With(r => r.Limit, 42)
                .With(r => r.EndDate, DateTime.Now.AddDays(30))
                .Create();
            var partner = CreateTestPartner();
            _repo.Setup(r => r.GetByIdAsync(id)).ReturnsAsync(partner);

            //act
            var res = await _controller.SetPartnerPromoCodeLimitAsync(id, request);

            //assert
            partner.PartnerLimits.Count.Should().Be(2);
            (partner.PartnerLimits as List<PartnerPromoCodeLimit>)[1].Limit.Should().Be(42);

            _repo.Verify(r => r.UpdateAsync(partner), Times.Once);

        }
    }
}
